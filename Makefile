##
## Makefile for arcade in /home/rigaud_b/rendu/cpp_arcade
##
## Made by Matthias RIGAUD
## Login   <rigaud_b@epitech.eu>
##
## Started on  Fri Mar 17 14:28:33 2017 Matthias RIGAUD
## Last update Sun Apr  9 18:19:27 2017 Michel Mancier
##

RM			= \rm -f

CC			= g++ -o3

CORE_NAME		= arcade

CACA_NAME		= lib_arcade_caca.so

SFML_NAME		= lib_arcade_sfml.so

LAPIN_NAME		= lib_arcade_lapin.so

NIBBLER_NAME		= lib_arcade_nibbler.so

QIX_NAME		= lib_arcade_qix.so

DIR			= sources

GAME_DIR		= $(DIR)/games

LIB_DIR			= $(DIR)/lib

CACA_SRCS		= $(LIB_DIR)/LibCaca.cpp \
			$(DIR)/Error.cpp

CACA_OBJS		= $(CACA_SRCS:.cpp=.o)

SFML_SRCS		= $(LIB_DIR)/LibSFML.cpp \
			$(DIR)/Error.cpp

SFML_OBJS		= $(SFML_SRCS:.cpp=.o)

LAPIN_SRCS		= $(LIB_DIR)/LibLapin.cpp \
			$(DIR)/Error.cpp

LAPIN_OBJS		= $(LAPIN_SRCS:.cpp=.o)

NIBBLER_SRCS		= $(GAME_DIR)/Nibbler.cpp

NIBBLER_OBJS		= $(NIBBLER_SRCS:.cpp=.o)

QIX_SRCS		= $(GAME_DIR)/Qix.cpp

QIX_OBJS		= $(QIX_SRCS:.cpp=.o)

CORE_DIR		= $(DIR)/core

CORE_SRCS		= $(CORE_DIR)/Main.cpp

CORE_OBJS		= $(CORE_SRCS:.cpp=.o)

SHEETIES		= $(CORE_OBJS) $(CACA_OBJS) $(SFML_OBJS) $(LAPIN_OBJS) $(NIBBLER_OBJS) $(QIX_OBJS)

CFLAGS			= -W -Wall -Werror -Wextra -Iincludes -fPIC

all:            	$(CACA_NAME) $(SFML_NAME) $(LAPIN_NAME) $(CORE_NAME) $(NIBBLER_NAME) $(QIX_NAME)

$(CORE_NAME):		$(CORE_OBJS)
			@echo -e "\e[0m"
			@$(CC) $(CORE_OBJS) -o $(CORE_NAME) -ldl
			@echo -e "\e[32mAll done ! ==>\e[33m" $(CORE_NAME) "\e[32mcreated !\e[0m"

$(CACA_NAME):		$(CACA_OBJS)
			@echo -e "\e[0m"
			@mkdir -p lib
			@$(CC) $(CACA_OBJS) -o lib/$(CACA_NAME) -shared -lcaca 2> /dev/null && \
			echo -e "\e[32mAll done ! ==>\e[33m" $(CACA_NAME) "\e[32mcreated !\e[0m" || \
			echo -e "\e[91;5m[ERROR]\e[25m Can't compile\e[33m" $(CACA_NAME) "\e[0m"

$(SFML_NAME):		$(SFML_OBJS)
			@echo -e "\e[0m"
			@mkdir -p lib
			@$(CC) $(SFML_OBJS) -o lib/$(SFML_NAME) -shared -lsfml-graphics 2> /dev/null && \
			echo -e "\e[32mAll done ! ==>\e[33m" $(SFML_NAME) "\e[32mcreated !\e[0m" || \
			echo -e "\e[91;5m[ERROR]\e[25m Can't compile\e[33m" $(SFML_NAME) "\e[0m"

$(LAPIN_NAME):		$(LAPIN_OBJS)
			@echo -e "\e[0m"
			@mkdir -p lib
			@$(CC) $(LAPIN_OBJS) -o lib/$(LAPIN_NAME) -shared -lsfml-graphics -L${HOME}/.froot/lib -llapin 2> /dev/null && \
			echo -e "\e[32mAll done ! ==>\e[33m" $(LAPIN_NAME) "\e[32mcreated !\e[0m" || \
			echo -e "\e[91;5m[ERROR]\e[25m Can't compile\e[33m" $(LAPIN_NAME) "\e[0m"

$(NIBBLER_NAME):	$(NIBBLER_OBJS)
			@echo -e "\e[0m"
			@mkdir -p games
			@$(CC) $(NIBBLER_OBJS) -o games/$(NIBBLER_NAME) -shared 2> /dev/null && \
			echo -e "\e[32mAll done ! ==>\e[33m" $(NIBBLER_NAME) "\e[32mcreated !\e[0m" || \
			echo -e "\e[91;5m[ERROR]\e[25m Can't compile\e[33m" $(NIBBLER_NAME) "\e[0m"

$(QIX_NAME):		$(QIX_OBJS)
			@echo -e "\e[0m"
			@mkdir -p games
			@$(CC) $(QIX_OBJS) -o games/$(QIX_NAME) -shared 2> /dev/null && \
			echo -e "\e[32mAll done ! ==>\e[33m" $(QIX_NAME) "\e[32mcreated !\e[0m" || \
			echo -e "\e[91;5m[ERROR]\e[25m Can't compile\e[33m" $(QIX_NAME) "\e[0m"

clean:
			@echo -en "\e[0mCleaning .o && .c~ files..."
			@$(RM) $(SHEETIES)
			@echo -e "	 [\e[32mOk !\e[0m]"

fclean:         	clean
			@echo -en "\e[39mCleaning executable..."
			@$(RM) $(CORE_NAME)
			@$(RM) -r lib games
			@echo -e "		 [\e[32mOk !\e[0m]"

re:             	fclean all

comp:           	re
			@echo -en "\e[0mCleaning .o && .c~ files..."
			@$(RM) $(SHEETIES)
			@echo -e "	 [\e[32mOk !\e[0m]"

.cpp.o:			%.cpp
			@$(CC) -c $< -o $@ $(CFLAGS) && \
			echo -e "\e[32m[OK]" $< "\e[93m"|| \
			echo -e "\e[91;5m[ERR]\e[25m" $< "\e[93m"

.PHONY:         	all clean fclean re comp
