#ifndef NIBBLER_HPP
# define NIBBLER_HPP

# include "Game.hpp"
# include <vector>
# include <unistd.h>
# include <iostream>
# include <ctime>
# include <cstdlib>
# include "Protocol.hpp"
# include <cstring>

class   Nibbler
{

public:

  struct        pos
  {
    pos(size_t x, size_t y): x(x), y(y) {}
    pos&        operator=(const pos&);
    pos&        operator+=(const pos&);
    bool        operator==(const pos&) const;
    bool        operator!=(const pos&) const;

    size_t      x;
    size_t      y;
  };

  class Fruit
  {

  public:

    Fruit(const pos&, ILib::Colors = ILib::RED);

    void        draw(ILib*) const;
    const pos&  getPos(void) const;
    void        changePos(void);

  private:

    ILib::Colors        _color;
    pos                 _pos;

  };


  class Snake
  {

  public:

    Snake(const pos&, ILib::Colors = ILib::GREEN, size_t = 4, size_t = 3);

    void        draw(ILib*) const;
    void        move(const pos& = pos(0, 0));
    void        specialMove(arcade::CommandType);
    bool        eatFruit(Fruit&);
    bool        isDead(void);
    void        writeWhereAmI(void) const;

    std::string getStringedLifes(void) const;

  private:

    void        _dead(void);

    std::vector<pos>    _cells;
    size_t              _nbCells;
    ILib::Colors        _color;
    pos                 _speed;
    size_t              _nbLife;
    const pos           _origPos;
    const size_t        _origCells;

  };


  explicit Nibbler(ILib*);
  ~Nibbler(void);

  void  setLib(ILib*);
  bool  isGameEnd(void) const;
  void  doTurn(void);

private:

  void  _initLib(void);
  void  _destroyLib(void);
  void  _getEvent(void);
  void  _getCmd(void);

  ILib*                 _lib;
  size_t                _score;
  Snake                 _snake;
  bool                  _end;
  ILib::Keys            _lastMove;
  clock_t               _begin;
  Fruit                 _fruit;
  arcade::CommandType   _cmd;

  /* const class variables */

  const static size_t   _winHeight;
  const static size_t   _winWidth;
  const static size_t   _intHeight;
  const static size_t   _intWidth;

};


#endif /* NIBBLER_HPP */
