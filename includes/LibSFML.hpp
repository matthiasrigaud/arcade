#ifndef LIBSFML_HPP
#define LIBSFML_HPP

# include "ILib.hpp"
# include "Error.hpp"
# include <SFML/Graphics.hpp>
# include <cmath>

# define VA(x)          ((x) < 0 ? -(x) : (x))

class Libsfml : public ILib
{
public:

  Libsfml(void);
  ~Libsfml(void);

  void  createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title);
  void  createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title, const size_t& cellSize);

  void  closeWindow(void);
  void  updateWindow(void);

  void  setCellSize(const size_t&);

  void  fillCell(const size_t& x, const size_t& y, const ILib::Colors&);
  void  fillWindow(const ILib::Colors&);

  void  drawLine(const size_t& x1, const size_t& y1, const size_t& x2, const size_t& y2, const ILib::Colors&);
  void  drawRect(const size_t&  x,
                 const size_t&  y,
                 const size_t&  width,
                 const size_t&  height,
                 const ILib::Colors&  ,
                 const bool&    fill = false);

  void  drawText(const size_t& x, const size_t& y, const std::string&, const ILib::Colors&, const size_t& size);

  void          drawSprite(const size_t& x, const size_t& y, const ILib::Sprite);
  ILib::Sprite  loadSprite(const std::string&, char equivalent = '\0', const ILib::Colors& = ILib::BLACK) const;
  void          deleteSprite(ILib::Sprite) const;

  const ILib::Event&    getNextEvent(void);

private:

  void  initColor(void);

  sf::RenderWindow      _window;
  size_t                _height;
  size_t                _width;
  size_t                _pixByCell;
  ILib::Event           _nextEvent;
  sf::Color             _colors[16];
  bool                  _modified;
};

#endif /* LIBSFML_HPP */
