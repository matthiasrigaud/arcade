#ifndef QIX_HPP
# define QIX_HPP

# include "Game.hpp"
# include <vector>
# include <unistd.h>
# include <iostream>
# include <ctime>
# include <cstdlib>
# include "Protocol.hpp"
# include <cstring>
# include <array>
# include <cmath>
# include <sstream>
# include <iomanip>

# define HEIGHT         40
# define WIDTH          60
# define INT_HEIGHT     10
# define FIELD_AREA     (HEIGHT - INT_HEIGHT - 2) * (WIDTH - 2)

# define CAR(x)         ((x) * (x))
# define VA(x)          ((x) < 0 ? -(x) : (x))

class   Qix
{
public:

  struct        pos
  {
    pos() {}
    pos(long int x, long int y): x(x), y(y) {}
    pos&        operator=(const pos&);
    pos&        operator+=(const pos&);
    pos&        operator-=(const pos&);
    pos         operator+(const pos&) const;
    pos         operator-(const pos&) const;
    bool        operator==(const pos&) const;
    bool        operator!=(const pos&) const;

    long int    x;
    long int    y;
  };

  enum class CellsType : int {
    WALKABLE = ILib::LIGHTCYAN,
    DRAWABLE = ILib::LIGHTGRAY,
    BLOCK = ILib::RED,
    ANTE_DRAWABLE = ILib::BLUE,
    BURNED = ILib::BLACK
  };

  typedef std::array<std::array<CellsType, WIDTH>, HEIGHT - INT_HEIGHT> Map;

  class BigFuckingMonster
  {

  public:

    BigFuckingMonster(const pos&, const pos&, ILib::Colors = ILib::DARKGRAY, double = 20);

    void        draw(ILib*) const;
    void        move(const Map&);
    double      reconquire(Map&, const pos& = pos(0, 0)) const;

    const pos&  getPosOne(void) const;
    const pos&  getPosTwo(void) const;

    void        printInterf(arcade::GetMap*) const;

  private:

    bool        _haveCollide(const Map&);

    pos                 _pos[2];
    pos                 _speed[2];
    ILib::Colors        _color;
    double              _maxSize;

  };

  class PlayerHunter
  {

  public:

    PlayerHunter(const pos&, ILib::Colors = ILib::LIGHTRED);

    void        draw(ILib*) const;
    void        move(const Map&);

    const pos&  getPos(void) const;
    void        printInterf(arcade::GetMap*) const;

  private:

    pos                 _pos;
    pos                 _last;
    ILib::Colors        _color;

  };

  typedef BigFuckingMonster     BFM;

  class Player
  {

  public:

    Player(const pos&, ILib::Colors = ILib::LIGHTBLUE, size_t = 3);

    void        draw(ILib*) const;
    double      move(const pos&, Map&, const bool&, const BFM&);
    double      specialMove(arcade::CommandType, Map&, const BFM&);
    bool        isDead(Map&, const BFM&, const std::array<PlayerHunter, 2>&);
    void        writeWhereAmI(void) const;

    std::string getStringedLifes(void) const;

  private:

    bool                _dead(Map&);

    pos                 _pos;
    ILib::Colors        _color;
    size_t              _nbLife;
    pos                 _orig;
    pos                 _burner;
    ILib::Colors        _burnColor;
    bool                _isBurned;
    bool                _canBurn;

  };

  explicit Qix(ILib*);
  ~Qix(void);

  void  setLib(ILib*);
  bool  isGameEnd(void) const;
  void  doTurn(void);

private:

  void  _initLib(void);
  void  _destroyLib(void);
  void  _getEvent(void);
  void  _getCmd(void);

  ILib*                 _lib;
  size_t                _score;
  Player                _player;
  bool                  _end;
  ILib::Keys            _lastMove;
  clock_t               _begin;
  arcade::CommandType   _cmd;
  Map                   _map;
  bool                  _canTrace;
  BFM                   _qix;
  double                _percent;

  std::array<PlayerHunter, 2>   _sparks;

  /* const class variables */

  const static size_t   _winHeight;
  const static size_t   _winWidth;
  const static size_t   _intHeight;
  const static size_t   _intWidth;

};

#endif /* QIX_HPP */
