#ifndef ILIB_HPP
# define ILIB_HPP

# include <string>
# include <cstdint>
# include <vector>

class   ILib
{

public:

  virtual ~ILib(void) {}

  /*********************
   *                   *
   *      Colors       *
   *                   *
   *********************/

  enum Colors {
#ifndef BLACK
    BLACK,
#else
    IBLACK,
#endif
#ifndef BLUE
    BLUE,
#else
    IBLUE,
#endif
#ifndef GREEN
    GREEN,
#else
    IGREEN,
#endif
    CYAN,
#ifndef RED
    RED,
#else
    IRED,
#endif
    MAGENTA,
    BROWN,
    LIGHTGRAY,
    DARKGRAY,
    LIGHTBLUE,
    LIGHTGREEN,
    LIGHTCYAN,
    LIGHTRED,
    LIGHTMAGENTA,
#ifndef YELLOW
    YELLOW,
#else
    IYELLOW,
#endif
#ifndef WHITE
    WHITE
#else
    IWHITE
#endif
  };

  /*********************
   *                   *
   *       Keys        *
   *                   *
   *********************/

  enum Keys {
    K_NULL,
    K_0,
    K_1,
    K_2,
    K_3,
    K_4,
    K_5,
    K_6,
    K_7,
    K_8,
    K_9,
    K_A,
    K_B,
    K_C,
    K_D,
    K_E,
    K_F,
    K_G,
    K_H,
    K_I,
    K_J,
    K_K,
    K_L,
    K_M,
    K_N,
    K_O,
    K_P,
    K_Q,
    K_R,
    K_S,
    K_T,
    K_U,
    K_V,
    K_W,
    K_X,
    K_Y,
    K_Z,
    K_LEFT,
    K_RIGHT,
    K_UP,
    K_DOWN,
    K_ESCAPE
  };

  enum EType {
    E_RELEASE,
    E_PRESS,
    E_NONE
  };

  struct        Event
  {
    Keys        key;
    EType       type;
  };

  /*********************
   *                   *
   *      Sprites      *
   *                   *
   *********************/

  typedef void* Sprite;

  /*********************
   *                   *
   * WINDOW MANAGEMENT *
   *                   *
   *********************/

  virtual void  createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title) = 0;
  virtual void  createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title, const size_t& cellSize) = 0;
  virtual void  closeWindow(void) = 0;
  virtual void  updateWindow(void) = 0;

  /*********************
   *                   *
   *  DRAW MANAGEMENT  *
   *                   *
   *********************/

  //
  // WARNING
  // X & Y must be coordinates in the Cell array
  //

  virtual void          setCellSize(const size_t&) = 0;

  virtual void          fillCell(const size_t& x, const size_t& y, const Colors&) = 0;
  virtual void          fillWindow(const Colors&) = 0;

  virtual void          drawLine(const size_t& x1, const size_t& y1, const size_t& x2, const size_t& y2, const Colors&) = 0;
  virtual void          drawRect(const size_t&  x,
                                 const size_t&  y,
                                 const size_t&  width,
                                 const size_t&  height,
                                 const Colors&  ,
                                 const bool&    fill = false) = 0;

  // size is a number of cell
  virtual void          drawText(const size_t& x, const size_t& y, const std::string&, const Colors&, const size_t& size) = 0;

  virtual void          drawSprite(const size_t& x, const size_t& y, const Sprite) = 0;
  #ifndef BLACK
  virtual Sprite        loadSprite(const std::string&, char equivalent = '\0', const Colors& = BLACK) const = 0;
  # else
  virtual Sprite        loadSprite(const std::string&, char equivalent = '\0', const Colors& = IBLACK) const = 0;
  #endif
  virtual void          deleteSprite(Sprite) const = 0;

  /*********************
   *                   *
   *   EVENT MANAGER   *
   *                   *
   *********************/

  virtual const Event&  getNextEvent(void) = 0;

};

extern "C" ILib *libGenerator(void);

#endif /* ILIB_HPP */
