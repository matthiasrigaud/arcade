#ifndef LIBCACA_HPP
# define LIBCACA_HPP

# include "ILib.hpp"
# include "Error.hpp"
# include <caca.h>

class   LibCaca : public ILib
{

public:

  LibCaca(void);
  ~LibCaca(void);

  void  createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title);
  void  createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title, const size_t& cellSize);
  void  closeWindow(void);
  void  updateWindow(void);

  void  setCellSize(const size_t&);

  void  fillCell(const size_t& x, const size_t& y, const ILib::Colors&);
  void  fillWindow(const ILib::Colors&);

  void  drawLine(const size_t& x1, const size_t& y1, const size_t& x2, const size_t& y2, const ILib::Colors&);
  void  drawRect(const size_t&  x,
                 const size_t&  y,
                 const size_t&  width,
                 const size_t&  height,
                 const ILib::Colors&  ,
                 const bool&    fill = false);

  void  drawText(const size_t& x, const size_t& y, const std::string&, const ILib::Colors&, const size_t& size);

  void          drawSprite(const size_t& x, const size_t& y, const ILib::Sprite);
  ILib::Sprite  loadSprite(const std::string&, char equivalent = '\0', const ILib::Colors& = ILib::BLACK) const;
  void          deleteSprite(ILib::Sprite) const;

  const ILib::Event&    getNextEvent(void);

  struct                Sprite
  {
    char                c;
    ILib::Colors        color;
  };

private:

  caca_canvas_t         *_cv;
  caca_display_t        *_dp;
  size_t                _height;
  size_t                _width;
  size_t                _pixByCell;
  bool                  _modified;
  ILib::Colors          _bg;
  ILib::Event           _nextEvent;

};


#endif /* LIBCACA_HPP */
