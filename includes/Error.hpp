#ifndef ERROR_HPP
# define ERROR_HPP

# include <string>
# include <exception>

class   Error: public std::exception
{

public:

  Error(const std::string&);
  ~Error(void) throw() {}

  const char*   what(void) const throw();

private:

  std::string   _msg;

};


#endif /* ERROR_HPP */
