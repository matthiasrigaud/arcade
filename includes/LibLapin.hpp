#ifndef LIBLAPIN_HPP
#define LIBLAPIN_HPP

# include <SFML/Graphics.hpp>
# include <lapin.h>
# include "ILib.hpp"
# include "Error.hpp"
# include <map>

# define VA(x)          ((x) < 0 ? -(x) : (x))

struct  bunny_window
{
  size_t                type;
  sf::RenderWindow      *window;
  ssize_t               width;
  ssize_t               height;
  const char            *window_name;
};

typedef struct          s_font
{
  t_bunny_pixelarray    *font;
  t_bunny_position      letter;
  int                   scale;
}                       t_font;

class LibLapin : public ILib
{
public:

  LibLapin(void);
  ~LibLapin(void);

  void  createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title);
  void  createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title, const size_t& cellSize);

  void  closeWindow(void);
  void  updateWindow(void);

  void  setCellSize(const size_t&);

  void  fillCell(const size_t& x, const size_t& y, const ILib::Colors&);
  void  fillWindow(const ILib::Colors&);

  void  drawLine(const size_t& x1, const size_t& y1, const size_t& x2, const size_t& y2, const ILib::Colors&);
  void  drawRect(const size_t&  x,
                 const size_t&  y,
                 const size_t&  width,
                 const size_t&  height,
                 const ILib::Colors&  ,
                 const bool&    fill = false);

  void  drawText(const size_t& x, const size_t& y, const std::string&, const ILib::Colors&, const size_t& size);

  void          drawSprite(const size_t& x, const size_t& y, const ILib::Sprite);
  ILib::Sprite  loadSprite(const std::string&, char equivalent = '\0', const ILib::Colors& = ILib::IBLACK) const;
  void          deleteSprite(ILib::Sprite) const;

  const ILib::Event&    getNextEvent(void);

private:

  void  initColor(void);

  t_bunny_window                                *_window;
  t_font                                        *_font;
  size_t                                        _height;
  size_t                                        _width;
  size_t                                        _pixByCell;
  ILib::Event                                   _nextEvent;
  static std::map<ILib::Colors, uint32_t>       _colors;
  bool                                          _modified;
};

#endif /* LIBSFML_HPP */
