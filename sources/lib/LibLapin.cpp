#include <alloca.h>
#include "LibLapin.hpp"


static unsigned int    pl_get_color(t_bunny_pixelarray *pix,
                                    int x,
                                    int y)
{
  t_bunny_position      pos;
  unsigned int          *pixel;

  if (x < 0 || y < 0 || x >= pix->clipable.clip_width ||
      y >= pix->clipable.clip_height)
    return (0);
  pos.x = x;
  pos.y = y;
  pixel = (unsigned int*)pix->pixels;
  return (pixel[pix->clipable.clip_width * pos.y + pos.x]);
}

static t_bunny_position getletterdim(t_bunny_pixelarray *font)
{
  t_bunny_position      letter;

  letter.x = font->clipable.buffer.width / 128;
  letter.y = font->clipable.buffer.height;
  return (letter);
}

LibLapin::LibLapin(void)
{
  _pixByCell = 0;
  initColor();
  _font = new t_font;
  _font->font = bunny_load_pixelarray("ressources/font.png");
  _font->scale = 1;
  _font->letter = getletterdim(_font->font);
}

LibLapin::~LibLapin(void)
{
}

void  LibLapin::createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title)
{
  if (!_pixByCell)
    throw(Error("Cell size must be greater than zero"));
  _height = nbYCells * _pixByCell;
  _width = nbXCells * _pixByCell;
  _window = bunny_start(_width, _height, false, title.c_str());
  _modified = true;
}

void  LibLapin::createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title, const size_t& cellSize)
{
  if (!(_pixByCell = cellSize))
    throw(Error("Cell size must be greater than zero"));
  _height = nbYCells * _pixByCell;
  _width = nbXCells * _pixByCell;
  _window = bunny_start(_width, _height, false, title.c_str());
  _modified = true;
}

void    LibLapin::closeWindow(void)
{
  bunny_stop(_window);
}

void    LibLapin::updateWindow(void)
{
  if (_modified)
    bunny_display(_window);
  _modified = false;
}

void    LibLapin::setCellSize(const size_t& cellSize)
{
  _pixByCell = cellSize;
}

void    LibLapin::fillCell(const size_t& x, const size_t& y, const ILib::Colors& color_id)
{
  t_bunny_vertex_array  *array = (t_bunny_vertex_array *)alloca(sizeof(*array) + 4 * sizeof(array->vertex[0]));
  array->length = 4;
  array->vertex[0].pos.x = ((double)(x * _pixByCell));
  array->vertex[0].pos.y = ((double)(y * _pixByCell));
  array->vertex[0].color = _colors[color_id];
  array->vertex[1].pos.x = ((double)((x + 1) * _pixByCell));
  array->vertex[1].pos.y = ((double)(y * _pixByCell));
  array->vertex[1].color = _colors[color_id];
  array->vertex[2].pos.x = ((double)(x * _pixByCell));
  array->vertex[2].pos.y = ((double)((y + 1) * _pixByCell));
  array->vertex[2].color = _colors[color_id];
  array->vertex[3].pos.x = ((double)((x + 1) * _pixByCell));
  array->vertex[3].pos.y = ((double)((y + 1) * _pixByCell));
  array->vertex[3].color = _colors[color_id];
  bunny_set_geometry(&_window->buffer, BGY_TRIANGLE_STRIP, array, NULL);
  _modified = true;
}

void    LibLapin::fillWindow(const ILib::Colors& color_id)
{
  bunny_clear(&_window->buffer, _colors[color_id]);
  _modified = true;
}

void            LibLapin::drawLine(const size_t& x1, const size_t& y1, const size_t& x2, const size_t& y2, const ILib::Colors& color_id)
{
  int           incrx = x1 - x2;
  int           incry = y1 - y2;
  int           curx;
  int           cury;
  double        divX;
  double        divY;

  if (VA(incrx) > VA(incry) && incrx)
    {
      divX = incrx / VA(incrx);
      divY = (double)incry / (double)(VA(incrx));
    }
  else
    {
      divY = incry / (VA(incry) ? VA(incry) : 1);
      divX = (double)incrx / (double)((VA(incry) ? VA(incry) : (VA(incrx) ? VA(incrx) : 1)));
    }
  for (size_t i = 0 ; (curx = x2 + i * divX) != x1 + divX && (cury = y2 + i * divY) != y1 + divY; i++)
    this->fillCell(curx, cury, color_id);
  _modified = true;
}

void    LibLapin::drawRect(const size_t&  x,
                          const size_t&  y,
                          const size_t&  width,
                          const size_t&  height,
                          const ILib::Colors&  color_id,
                          const bool&    fill)
{
  if (fill)
    {
      size_t                   i;
      t_bunny_position      draw;

      i = 0;
      while (i < width * height)
        {
          draw.x = x + i % width;
          draw.y = y + i / width;
          fillCell(draw.x, draw.y, color_id);
          ++i;
        }
    }
  else
    {
      t_bunny_position      axis;

      axis.y = y;
      while ((size_t)axis.y < (height + y))
        {
          axis.x = x;
          while ((size_t)axis.x < (x + width))
            {
              if ((size_t)axis.x == x ||
                  (size_t)axis.x == (x + width - 1) ||
                  (size_t)axis.y == y || (size_t)axis.y == (y + height - 1))
                fillCell(axis.x, axis.y, color_id);
              axis.x++;
            }
          axis.y++;
        }
    }
  _modified = true;
}

static void    print_scale(t_bunny_buffer *out,
                           t_bunny_position *pos,
                           t_color *color,
                           int scale)
{
  t_bunny_position      bis;
  int   yx[2];

  yx[0] = -1;
  while (++yx[0] < scale)
    {
      yx[1] = -1;
      while (++yx[1] < scale)
        {
          bis.x = pos->x + yx[1];
          bis.y = pos->y + yx[0];
          bunny_set_pixel(out, bis, color->full);
        }
    }
}

static void     draw_letter(t_bunny_buffer *out,
                            t_font *font,
                            char c,
                            t_bunny_position *bis)
{
  int                   i;
  int                   yx[2];
  t_color               color;
  t_bunny_position      pos;

  i = c * font->letter.x;
  yx[0] = -1;
  while (++yx[0] < font->letter.y)
    {
      yx[1] = -1;
      while (++yx[1] < font->letter.x)
        {
          color.full = pl_get_color(font->font, yx[1] + i, yx[0]);
          pos.x = bis->x + yx[1] * font->scale;
          pos.y = bis->y + yx[0] * font->scale;
          if (color.argb[ALPHA_CMP] > 0)
            print_scale(out, &pos, &color, font->scale);
        }
    }
}

void    LibLapin::drawText(const size_t& x, const size_t& y, const std::string& str, const ILib::Colors& color_id, const size_t& size)
{
  (void)color_id;
  (void)size;

  int                   i;
  t_bunny_position      bis;

  bis.x = x * _pixByCell + 5;
  bis.y = y * _pixByCell + 5;
  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] == '\n')
        {
          bis.x = x;
          bis.y += _font->letter.y * _font->scale + 1;;
        }
      else
        {
          draw_letter(&_window->buffer, _font, str[i], &bis);
          bis.x += _font->letter.x * _font->scale + 1;
        }
      ++i;

    }
  _modified = true;
}

void            LibLapin::drawSprite(const size_t& x, const size_t& y, const ILib::Sprite sprite)
{
  (void)x;
  (void)y;
  (void)sprite;
}

ILib::Sprite    LibLapin::loadSprite(const std::string& path, char equivalent, const ILib::Colors& color_id) const
{
  (void)path;
  (void)equivalent;
  (void)color_id;
  return (NULL);
}

void    LibLapin::deleteSprite(ILib::Sprite sprite) const
{
  delete (Sprite *)sprite;
}

const ILib::Event&      LibLapin::getNextEvent(void)
{
  sf::Event             event;

  struct bunny_window   *win = (struct bunny_window *)_window;

  _nextEvent.type = ILib::E_NONE;
  if (win->window->pollEvent(event))
    {
      if (event.type == sf::Event::KeyPressed)
        _nextEvent.type = ILib::E_PRESS;
      else if (event.type == sf::Event::KeyReleased)
        _nextEvent.type = ILib::E_RELEASE;
      switch (event.key.code)
        {
        case sf::Keyboard::Up:
          _nextEvent.key = ILib::K_UP;
          break;
        case sf::Keyboard::Down:
          _nextEvent.key = ILib::K_DOWN;
          break;
        case sf::Keyboard::Right:
          _nextEvent.key = ILib::K_RIGHT;
          break;
        case sf::Keyboard::Left:
          _nextEvent.key = ILib::K_LEFT;
          break;
        case sf::Keyboard::Escape:
          _nextEvent.key = ILib::K_ESCAPE;
          break;
        case sf::Keyboard::Num0:
          _nextEvent.key = ILib::K_0;
          break;
        case sf::Keyboard::Num1:
          _nextEvent.key = ILib::K_1;
          break;
        case sf::Keyboard::Num2:
          _nextEvent.key = ILib::K_2;
          break;
        case sf::Keyboard::Num3:
          _nextEvent.key = ILib::K_3;
          break;
        case sf::Keyboard::Num4:
          _nextEvent.key = ILib::K_4;
          break;
        case sf::Keyboard::Num5:
          _nextEvent.key = ILib::K_5;
          break;
        case sf::Keyboard::Num6:
          _nextEvent.key = ILib::K_6;
          break;
        case sf::Keyboard::Num7:
          _nextEvent.key = ILib::K_7;
          break;
        case sf::Keyboard::Num8:
          _nextEvent.key = ILib::K_8;
          break;
        case sf::Keyboard::Num9:
          _nextEvent.key = ILib::K_9;
          break;
        case sf::Keyboard::A:
          _nextEvent.key = ILib::K_A;
          break;
        case sf::Keyboard::B:
          _nextEvent.key = ILib::K_B;
          break;
        case sf::Keyboard::C:
          _nextEvent.key = ILib::K_C;
          break;
        case sf::Keyboard::D:
          _nextEvent.key = ILib::K_D;
          break;
        case sf::Keyboard::E:
          _nextEvent.key = ILib::K_E;
          break;
        case sf::Keyboard::F:
          _nextEvent.key = ILib::K_F;
          break;
        case sf::Keyboard::G:
          _nextEvent.key = ILib::K_G;
          break;
        case sf::Keyboard::H:
          _nextEvent.key = ILib::K_H;
          break;
        case sf::Keyboard::I:
          _nextEvent.key = ILib::K_I;
          break;
        case sf::Keyboard::J:
          _nextEvent.key = ILib::K_J;
          break;
        case sf::Keyboard::K:
          _nextEvent.key = ILib::K_K;
          break;
        case sf::Keyboard::L:
          _nextEvent.key = ILib::K_L;
          break;
        case sf::Keyboard::M:
          _nextEvent.key = ILib::K_M;
          break;
        case sf::Keyboard::N:
          _nextEvent.key = ILib::K_N;
          break;
        case sf::Keyboard::O:
          _nextEvent.key = ILib::K_O;
          break;
        case sf::Keyboard::P:
          _nextEvent.key = ILib::K_P;
          break;
        case sf::Keyboard::Q:
          _nextEvent.key = ILib::K_Q;
          break;
        case sf::Keyboard::R:
          _nextEvent.key = ILib::K_R;
          break;
        case sf::Keyboard::S:
          _nextEvent.key = ILib::K_S;
          break;
        case sf::Keyboard::T:
          _nextEvent.key = ILib::K_T;
          break;
        case sf::Keyboard::U:
          _nextEvent.key = ILib::K_U;
          break;
        case sf::Keyboard::V:
          _nextEvent.key = ILib::K_V;
          break;
        case sf::Keyboard::W:
          _nextEvent.key = ILib::K_W;
          break;
        case sf::Keyboard::X:
          _nextEvent.key = ILib::K_X;
          break;
        case sf::Keyboard::Y:
          _nextEvent.key = ILib::K_Y;
          break;
        case sf::Keyboard::Z :
          _nextEvent.key = ILib::K_Z;
          break;
        default:
          _nextEvent.key = ILib::K_NULL;
        }
    }
  return (_nextEvent);
}

ILib    *libGenerator(void)
{
  return (new LibLapin);
}

void    LibLapin::initColor()
{
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::IBLACK, ((uint32_t)0xFF000000)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::IBLUE, ((uint32_t)0xFF0000FF)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::IGREEN, ((uint32_t)0xFF00FF00)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::CYAN, ((uint32_t)0xFF00FFFF)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::IRED, ((uint32_t)0xFFFF0000)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::MAGENTA, ((uint32_t)0xFFFF00FF)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::BROWN, ((uint32_t)0xFF835008)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::LIGHTGRAY, ((uint32_t)0xFFD5D5D5)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::DARKGRAY, ((uint32_t)0xFF5D5D5D)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::LIGHTBLUE, ((uint32_t)0xFF7B7BFF)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::LIGHTGREEN, ((uint32_t)0xFF7BFF7B)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::LIGHTCYAN, ((uint32_t)0xFF7BFFFF)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::LIGHTRED, ((uint32_t)0xFFFF7B7B)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::LIGHTMAGENTA, ((uint32_t)0xFFFF7BFF)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::IYELLOW, ((uint32_t)0xFFFFFF00)));
  _colors.insert(std::pair<ILib::Colors, uint32_t>(ILib::IWHITE, ((uint32_t)0xFFFFFFFF)));
}

std::map<ILib::Colors, uint32_t> LibLapin::_colors = std::map<ILib::Colors, uint32_t>();
