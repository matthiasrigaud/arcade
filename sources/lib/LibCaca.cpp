# include "LibCaca.hpp"

LibCaca::LibCaca(void)
{
  _pixByCell = 0;
  _dp = NULL;
  _cv = NULL;
  _bg = ILib::BLACK;
  _modified = true;
}

LibCaca::~LibCaca(void)
{
  if (_dp)
    caca_free_display(_dp);
  if (_cv)
    caca_free_canvas(_cv);
}

void    LibCaca::createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title)
{
  if (!_pixByCell)
    throw(Error("Cellule size must be greater than zero"));
  _height = nbYCells;
  _width = nbXCells;
  if (!(_cv = caca_create_canvas(nbXCells, nbYCells)))
    throw(Error("Error when create canvas"));
  if (!(_dp = caca_create_display(_cv)))
    throw(Error("Error when create display"));
  caca_set_display_title(_dp, title.c_str());
}

void    LibCaca::createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title, const size_t& cellSize)
{
  if (!(_pixByCell = cellSize))
    throw(Error("Cellule size must be greater than zero"));
  _height = nbYCells;
  _width = nbXCells;
  if (!(_cv = caca_create_canvas(nbXCells, nbYCells)))
    throw(Error("Error when create canvas"));
  if (!(_dp = caca_create_display(_cv)))
    throw(Error("Error when create display"));
  caca_set_display_title(_dp, title.c_str());
}

void    LibCaca::closeWindow(void)
{
  caca_free_display(_dp);
  caca_free_canvas(_cv);
  _dp = NULL;
  _cv = NULL;
}

void    LibCaca::updateWindow(void)
{
  if (_modified)
    caca_refresh_display(_dp);
  _modified = false;
}

void    LibCaca::setCellSize(const size_t& cellSize)
{
  _pixByCell = cellSize;
}

void    LibCaca::fillCell(const size_t& x, const size_t& y, const ILib::Colors& color)
{
  caca_set_color_ansi(_cv, CACA_DEFAULT, color);
  caca_put_str(_cv, x, y, " ");
  _modified = true;
}

void    LibCaca::fillWindow(const ILib::Colors& color)
{
  caca_set_color_ansi(_cv, CACA_DEFAULT, color);
  caca_fill_box(_cv, 0, 0, _width, _height, ' ');
  _bg = color;
  _modified = true;
}

void            LibCaca::drawLine(const size_t& x1, const size_t& y1, const size_t& x2, const size_t& y2, const ILib::Colors& color)
{
  caca_set_color_ansi(_cv, color, _bg);
  caca_draw_thin_line(_cv, x1, y1, x2, y2);
  _modified = true;
}

void    LibCaca::drawRect(const size_t&  x,
                          const size_t&  y,
                          const size_t&  width,
                          const size_t&  height,
                          const ILib::Colors&  color,
                          const bool&    fill)
{
  if (fill)
    {
      caca_set_color_ansi(_cv, CACA_DEFAULT, color);
      caca_fill_box(_cv, x, y, width, height, ' ');
    }
  else
    {
      caca_set_color_ansi(_cv, color, _bg);
      caca_draw_thin_box(_cv, x, y, width, height);
    }
  _modified = true;
}

void    LibCaca::drawText(const size_t& x, const size_t& y, const std::string& text, const ILib::Colors& color, const size_t& size)
{
  (void)size;
  caca_set_color_ansi(_cv, color, _bg);
  caca_printf(_cv, x, y, "%s", text.c_str());
  _modified = true;
}

void            LibCaca::drawSprite(const size_t& x, const size_t& y, const ILib::Sprite sprite)
{
  Sprite        *cacaSprite = (Sprite *)sprite;

  caca_set_color_ansi(_cv, cacaSprite->color, _bg);
  caca_printf(_cv, x, y, "%c", cacaSprite->c);
  _modified = true;
}

ILib::Sprite    LibCaca::loadSprite(const std::string& path, char equivalent, const ILib::Colors& color) const
{
  Sprite        *sprite = new Sprite;

  (void)path;
  sprite->c = equivalent;
  sprite->color = color;
  return (sprite);
}

void    LibCaca::deleteSprite(ILib::Sprite sprite) const
{
  delete (Sprite *)sprite;
}

const ILib::Event&      LibCaca::getNextEvent(void)
{
  caca_event_t  event;

  caca_get_event(_dp, CACA_EVENT_KEY_PRESS | CACA_EVENT_KEY_RELEASE, &event, 0);
  _nextEvent.type = ILib::E_NONE;
  if (event.type == CACA_EVENT_KEY_PRESS)
    _nextEvent.type = ILib::E_PRESS;
  else if (event.type == CACA_EVENT_KEY_RELEASE)
    _nextEvent.type = ILib::E_RELEASE;
  switch (event.data.key.ch)
    {
    case 273:
      _nextEvent.key = ILib::K_UP;
      break;
    case 274:
      _nextEvent.key = ILib::K_DOWN;
      break;
    case 276:
      _nextEvent.key = ILib::K_RIGHT;
      break;
    case 275:
      _nextEvent.key = ILib::K_LEFT;
      break;
    case 27:
      _nextEvent.key = ILib::K_ESCAPE;
      break;
    case 48:
    case 224:
      _nextEvent.key = ILib::K_0;
      break;
    case 49:
    case 38:
      _nextEvent.key = ILib::K_1;
      break;
    case 50:
    case 233:
      _nextEvent.key = ILib::K_2;
      break;
    case 51:
    case 34:
      _nextEvent.key = ILib::K_3;
      break;
    case 52:
    case 39:
      _nextEvent.key = ILib::K_4;
      break;
    case 53:
    case 40:
      _nextEvent.key = ILib::K_5;
      break;
    case 54:
    case 45:
      _nextEvent.key = ILib::K_6;
      break;
    case 55:
    case 232:
      _nextEvent.key = ILib::K_7;
      break;
    case 56:
    case 95:
      _nextEvent.key = ILib::K_8;
      break;
    case 57:
    case 231:
      _nextEvent.key = ILib::K_9;
      break;
    case 'a':
    case 'A':
      _nextEvent.key = ILib::K_A;
      break;
    case 'b':
    case 'B':
      _nextEvent.key = ILib::K_B;
      break;
    case 'c':
    case 'C':
      _nextEvent.key = ILib::K_C;
      break;
    case 'd':
    case 'D':
      _nextEvent.key = ILib::K_D;
      break;
    case 'e':
    case 'E':
      _nextEvent.key = ILib::K_E;
      break;
    case 'f':
    case 'F':
      _nextEvent.key = ILib::K_F;
      break;
    case 'g':
    case 'G':
      _nextEvent.key = ILib::K_G;
      break;
    case 'h':
    case 'H':
      _nextEvent.key = ILib::K_H;
      break;
    case 'i':
    case 'I':
      _nextEvent.key = ILib::K_I;
      break;
    case 'j':
    case 'J':
      _nextEvent.key = ILib::K_J;
      break;
    case 'k':
    case 'K':
      _nextEvent.key = ILib::K_K;
      break;
    case 'l':
    case 'L':
      _nextEvent.key = ILib::K_L;
      break;
    case 'm':
    case 'M':
      _nextEvent.key = ILib::K_M;
      break;
    case 'n':
    case 'N':
      _nextEvent.key = ILib::K_N;
      break;
    case 'o':
    case 'O':
      _nextEvent.key = ILib::K_O;
      break;
    case 'p':
    case 'P':
      _nextEvent.key = ILib::K_P;
      break;
    case 'q':
    case 'Q':
      _nextEvent.key = ILib::K_Q;
      break;
    case 'r':
    case 'R':
      _nextEvent.key = ILib::K_R;
      break;
    case 's':
    case 'S':
      _nextEvent.key = ILib::K_S;
      break;
    case 't':
    case 'T':
      _nextEvent.key = ILib::K_T;
      break;
    case 'u':
    case 'U':
      _nextEvent.key = ILib::K_U;
      break;
    case 'v':
    case 'V':
      _nextEvent.key = ILib::K_V;
      break;
    case 'w':
    case 'W':
      _nextEvent.key = ILib::K_W;
      break;
    case 'x':
    case 'X':
      _nextEvent.key = ILib::K_X;
      break;
    case 'y':
    case 'Y':
      _nextEvent.key = ILib::K_Y;
      break;
    case 'z':
    case 'Z':
      _nextEvent.key = ILib::K_Z;
      break;
    default:
      _nextEvent.key = ILib::K_NULL;
    }
  return (_nextEvent);
}

ILib    *libGenerator(void)
{
  return (new LibCaca);
}
