#include "LibSFML.hpp"

Libsfml::Libsfml(void)
{
  _pixByCell = 0;
  initColor();
}

Libsfml::~Libsfml(void)
{
}

void  Libsfml::createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title)
{
  if (!_pixByCell)
    throw(Error("Cellule size must be greater than zero"));
  _height = nbYCells;
  _width = nbXCells;
  _window.create(sf::VideoMode(nbXCells, nbYCells), title, sf::Style::None);
  _modified = true;
}

void  Libsfml::createWindow(const size_t& nbXCells, const size_t& nbYCells, const std::string& title, const size_t& cellSize)
{
  if (!(_pixByCell = cellSize))
    throw(Error("Cellule size must be greater than zero"));
  _height = nbYCells * _pixByCell;
  _width = nbXCells * _pixByCell;
  _window.create(sf::VideoMode(_width, _height), title, sf::Style::None | sf::Style::Titlebar);
  _modified = true;
  // _window.setFramerateLimit(60);
}

void    Libsfml::closeWindow(void)
{
  _window.close();
}

void    Libsfml::updateWindow(void)
{
  if (_modified)
    _window.display();
  _modified = false;
}

void    Libsfml::setCellSize(const size_t& cellSize)
{
  _pixByCell = cellSize;
}

void    Libsfml::fillCell(const size_t& x, const size_t& y, const ILib::Colors& color_id)
{
  sf::RectangleShape cell(sf::Vector2f(_pixByCell, _pixByCell));

  cell.setPosition(x * _pixByCell, y * _pixByCell);
  cell.setFillColor(_colors[color_id]);
  _window.draw(cell);
  _modified = true;
}

void    Libsfml::fillWindow(const ILib::Colors& color_id)
{
  _window.clear(_colors[color_id]);
  _modified = true;
}

void            Libsfml::drawLine(const size_t& x1, const size_t& y1, const size_t& x2, const size_t& y2, const ILib::Colors& color_id)
{
  int           incrx = x1 - x2;
  int           incry = y1 - y2;
  int           curx;
  int           cury;
  double        divX;
  double        divY;

  if (VA(incrx) > VA(incry) && incrx)
    {
      divX = incrx / VA(incrx);
      divY = (double)incry / (double)(VA(incrx));
    }
  else
    {
      divY = incry / (VA(incry) ? VA(incry) : 1);
      divX = (double)incrx / (double)((VA(incry) ? VA(incry) : (VA(incrx) ? VA(incrx) : 1)));
    }
  for (size_t i = 0 ; (curx = x2 + i * divX) != x1 + divX && (cury = y2 + i * divY) != y1 + divY; i++)
    this->fillCell(curx, cury, color_id);
  _modified = true;
}

void    Libsfml::drawRect(const size_t&  x,
                          const size_t&  y,
                          const size_t&  width,
                          const size_t&  height,
                          const ILib::Colors&  color_id,
                          const bool&    fill)
{
  sf::RectangleShape rect(sf::Vector2f(width * _pixByCell, height * _pixByCell));
  rect.setPosition(x * _pixByCell, y * _pixByCell);
  if (fill)
    rect.setFillColor(_colors[color_id]);
  else
    {
      rect.setFillColor(sf::Color::Transparent);
      this->drawRect(x, y, width, 1, color_id, true);
      this->drawRect(x, y + height - 1, width, 1, color_id, true);
      this->drawRect(x, y, 1, height, color_id, true);
      this->drawRect(x + width - 1, y, 1, height, color_id, true);
    }
  _window.draw(rect);
  _modified = true;
}

void    Libsfml::drawText(const size_t& x, const size_t& y, const std::string& str, const ILib::Colors& color_id, const size_t& size)
{
  sf::Text text;
  sf::Font font;

  if (!font.loadFromFile("ressources/arial.ttf"))
    throw(Error("Error on loading font"));
  text.setFont(font);
  text.setString(str);
  text.setPosition(x * _pixByCell, y * _pixByCell);
  text.setCharacterSize(size * _pixByCell);
  text.setColor(_colors[color_id]);
  _window.draw(text);
  _modified = true;
}

void            Libsfml::drawSprite(const size_t& x, const size_t& y, const ILib::Sprite sprite)
{
  sf::Sprite    *sfSprite = (sf::Sprite *)sprite;

  sfSprite->setPosition(x * _pixByCell, y * _pixByCell);
  _window.draw(*sfSprite);
  _modified = true;
}

ILib::Sprite    Libsfml::loadSprite(const std::string& path, char equivalent, const ILib::Colors& color_id) const
{
  sf::Texture   texture;
  sf::Sprite    *sprite = new sf::Sprite();

  (void) equivalent;
  if (!texture.loadFromFile(path))
    throw(Error("Error on loading texture: " + path));
  sprite->setTexture(texture);
  sprite->setColor(_colors[color_id]);
  return (sprite);
}

void    Libsfml::deleteSprite(ILib::Sprite sprite) const
{
  delete (Sprite *)sprite;
}

const ILib::Event&      Libsfml::getNextEvent(void)
{
  sf::Event             event;

  _nextEvent.type = ILib::E_NONE;
  if (_window.pollEvent(event))
    {
      if (event.type == sf::Event::KeyPressed)
        _nextEvent.type = ILib::E_PRESS;
      else if (event.type == sf::Event::KeyReleased)
        _nextEvent.type = ILib::E_RELEASE;
      switch (event.key.code)
        {
        case sf::Keyboard::Up:
          _nextEvent.key = ILib::K_UP;
          break;
        case sf::Keyboard::Down:
          _nextEvent.key = ILib::K_DOWN;
          break;
        case sf::Keyboard::Right:
          _nextEvent.key = ILib::K_RIGHT;
          break;
        case sf::Keyboard::Left:
          _nextEvent.key = ILib::K_LEFT;
          break;
        case sf::Keyboard::Escape:
          _nextEvent.key = ILib::K_ESCAPE;
          break;
        case sf::Keyboard::Num0:
          _nextEvent.key = ILib::K_0;
          break;
        case sf::Keyboard::Num1:
          _nextEvent.key = ILib::K_1;
          break;
        case sf::Keyboard::Num2:
          _nextEvent.key = ILib::K_2;
          break;
        case sf::Keyboard::Num3:
          _nextEvent.key = ILib::K_3;
          break;
        case sf::Keyboard::Num4:
          _nextEvent.key = ILib::K_4;
          break;
        case sf::Keyboard::Num5:
          _nextEvent.key = ILib::K_5;
          break;
        case sf::Keyboard::Num6:
          _nextEvent.key = ILib::K_6;
          break;
        case sf::Keyboard::Num7:
          _nextEvent.key = ILib::K_7;
          break;
        case sf::Keyboard::Num8:
          _nextEvent.key = ILib::K_8;
          break;
        case sf::Keyboard::Num9:
          _nextEvent.key = ILib::K_9;
          break;
        case sf::Keyboard::A:
          _nextEvent.key = ILib::K_A;
          break;
        case sf::Keyboard::B:
          _nextEvent.key = ILib::K_B;
          break;
        case sf::Keyboard::C:
          _nextEvent.key = ILib::K_C;
          break;
        case sf::Keyboard::D:
          _nextEvent.key = ILib::K_D;
          break;
        case sf::Keyboard::E:
          _nextEvent.key = ILib::K_E;
          break;
        case sf::Keyboard::F:
          _nextEvent.key = ILib::K_F;
          break;
        case sf::Keyboard::G:
          _nextEvent.key = ILib::K_G;
          break;
        case sf::Keyboard::H:
          _nextEvent.key = ILib::K_H;
          break;
        case sf::Keyboard::I:
          _nextEvent.key = ILib::K_I;
          break;
        case sf::Keyboard::J:
          _nextEvent.key = ILib::K_J;
          break;
        case sf::Keyboard::K:
          _nextEvent.key = ILib::K_K;
          break;
        case sf::Keyboard::L:
          _nextEvent.key = ILib::K_L;
          break;
        case sf::Keyboard::M:
          _nextEvent.key = ILib::K_M;
          break;
        case sf::Keyboard::N:
          _nextEvent.key = ILib::K_N;
          break;
        case sf::Keyboard::O:
          _nextEvent.key = ILib::K_O;
          break;
        case sf::Keyboard::P:
          _nextEvent.key = ILib::K_P;
          break;
        case sf::Keyboard::Q:
          _nextEvent.key = ILib::K_Q;
          break;
        case sf::Keyboard::R:
          _nextEvent.key = ILib::K_R;
          break;
        case sf::Keyboard::S:
          _nextEvent.key = ILib::K_S;
          break;
        case sf::Keyboard::T:
          _nextEvent.key = ILib::K_T;
          break;
        case sf::Keyboard::U:
          _nextEvent.key = ILib::K_U;
          break;
        case sf::Keyboard::V:
          _nextEvent.key = ILib::K_V;
          break;
        case sf::Keyboard::W:
          _nextEvent.key = ILib::K_W;
          break;
        case sf::Keyboard::X:
          _nextEvent.key = ILib::K_X;
          break;
        case sf::Keyboard::Y:
          _nextEvent.key = ILib::K_Y;
          break;
        case sf::Keyboard::Z:
          _nextEvent.key = ILib::K_Z;
          break;
        default:
          _nextEvent.key = ILib::K_NULL;
        }
    }
  return (_nextEvent);
}

ILib    *libGenerator(void)
{
  return (new Libsfml);
}

void    Libsfml::initColor()
{
  _colors[BLACK] = sf::Color(0, 0, 0);
  _colors[BLUE] = sf::Color(0, 0, 238);
  _colors[GREEN] = sf::Color(0, 205, 0);
  _colors[CYAN] = sf::Color(0, 205, 205);
  _colors[RED] = sf::Color(205, 0, 0);
  _colors[MAGENTA] = sf::Color(205, 0, 205);
  _colors[BROWN] = sf::Color(170, 85, 0);
  _colors[LIGHTGRAY] = sf::Color(229, 229, 229);
  _colors[DARKGRAY] = sf::Color(127, 127, 127);
  _colors[LIGHTBLUE] = sf::Color(92, 92, 255);
  _colors[LIGHTGREEN] = sf::Color(0, 255, 0);
  _colors[LIGHTCYAN] = sf::Color(0, 205, 205);
  _colors[LIGHTRED] = sf::Color(255, 0, 0);
  _colors[LIGHTMAGENTA] = sf::Color(255, 0, 255);
  _colors[YELLOW] =sf::Color(255, 255, 0);
  _colors[WHITE] = sf::Color(255, 255, 255);
}
