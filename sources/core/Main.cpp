# include "ILib.hpp"
# include <dlfcn.h>

typedef ILib*   (*generatorPtr)(void);
typedef void    (*gameLauncher)(ILib *);

int             main(int ac, char **av)
{
  ILib          *lib;
  // ILib::Sprite  sprite;
  void          *handle;
  generatorPtr  generator;
  gameLauncher  game;

  if (ac != 3 || !(handle = dlopen(av[1], RTLD_LAZY)) || !(generator = (generatorPtr)dlsym(handle, "libGenerator")) ||
      !(handle = dlopen(av[2], RTLD_LAZY)) || !(game = (gameLauncher)dlsym(handle, "GPlay")))
    return (1);
  lib = generator();
  // lib->createWindow(50, 50, "Cookie", 10);
  // lib->fillWindow(ILib::BLUE);
  // lib->fillCell(5, 5, ILib::RED);
  // lib->drawLine(0, 5, 15, 23, ILib::WHITE);
  // lib->drawText(0, 10, "COOKIE", ILib::GREEN, 42);
  // lib->drawRect(25, 20, 15, 10, ILib::BLACK, true);
  // lib->drawRect(20, 25, 10, 15, ILib::YELLOW);
  // sprite = lib->loadSprite("lolololo", 'G', ILib::LIGHTBLUE);
  // lib->drawSprite(30, 30, sprite);
  // lib->deleteSprite(sprite);
  // while (lib->getNextEvent().key != ILib::K_ESCAPE)
  //   lib->updateWindow();
  // lib->closeWindow();
  game(lib);
  delete lib;
  return (0);
}
