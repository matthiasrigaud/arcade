# include "Error.hpp"

Error::Error(const std::string& msg)
{
  _msg = msg;
}

const char*     Error::what(void) const throw()
{
  return (_msg.c_str());
}
