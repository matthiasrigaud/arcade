# include "Nibbler.hpp"

/*
**
** Init static constant of Nibbler
**
*/

const size_t    Nibbler::_winHeight = 40;
const size_t    Nibbler::_winWidth = 60;
const size_t    Nibbler::_intHeight = 10;
const size_t    Nibbler::_intWidth = _winWidth;

/*
**
** Operator overloading for pos struct
**
*/

bool    Nibbler::pos::operator!=(const pos& other) const
{
  return (!(this->x == other.x && this->y == other.y));
}

bool    Nibbler::pos::operator==(const pos& other) const
{
  return ((this->x == other.x && this->y == other.y));
}

Nibbler::pos&   Nibbler::pos::operator=(const pos& other)
{
  this->x = other.x;
  this->y = other.y;
  return (*this);
}

Nibbler::pos&   Nibbler::pos::operator+=(const pos& other)
{
  this->x += other.x;
  this->y += other.y;
  return (*this);
}

/*
**
** Fruit functions
**
*/

Nibbler::Fruit::Fruit(const pos& startPos, ILib::Colors color): _pos(startPos)
{
  _color = color;
}

void    Nibbler::Fruit::draw(ILib *lib) const
{
  if (lib)
    lib->fillCell(_pos.x, _pos.y, _color);
}

const Nibbler::pos&     Nibbler::Fruit::getPos(void) const
{
  return (_pos);
}

void    Nibbler::Fruit::changePos(void)
{
  _pos = pos(rand() % (_winWidth - 2) + 1, rand() % (_winHeight - _intHeight - 2) + 1);
}

/*
**
** Snake functions
**
*/

Nibbler::Snake::Snake(const pos& startPos, ILib::Colors color, size_t size, size_t lifes): _speed(pos(0, -1)),
                                                                                           _origPos(startPos),
                                                                                           _origCells(size)
{
  _cells.push_back(startPos);
  _color = color;
  _nbCells = size;
  _nbLife = lifes;
}

void    Nibbler::Snake::draw(ILib *lib) const
{
  if (!lib)
    return ;
  for (size_t i = 0 ; i < _cells.size() ; i++)
    lib->fillCell(_cells[i].x, _cells[i].y, _color);
}

void    Nibbler::Snake::move(const pos& movement)
{
  if (movement != _speed && (movement.y || movement.x))
    _speed = movement;
  if (_cells.size() < _nbCells)
    _cells.push_back(pos(0, 0));
  for (size_t i = _cells.size() - 1 ; i > 0 ; i--)
    _cells[i] = _cells[i - 1];
  _cells[0] += _speed;
}

void    Nibbler::Snake::specialMove(arcade::CommandType cmd)
{
  // pos   tmp = _speed;

  // switch (cmd)
  //   {
  //   case arcade::CommandType::GO_RIGHT:
  //     _speed.x = -1 * tmp.y;
  //     _speed.y = tmp.x;
  //     break;
  //   case arcade::CommandType::GO_LEFT:
  //     _speed.y = -1 * tmp.x;
  //     _speed.x = tmp.y;
  //     break;
  //   default:
  //     break;
  //   }
  switch (cmd)
    {
    case arcade::CommandType::GO_UP:
      _speed.x = 0;
      _speed.y = -1;
      break;
    case arcade::CommandType::GO_DOWN:
      _speed.x = 0;
      _speed.y = 1;
      break;
    case arcade::CommandType::GO_LEFT:
      _speed.x = -1;
      _speed.y = 0;
      break;
    case arcade::CommandType::GO_RIGHT:
      _speed.x = 1;
      _speed.y = 0;
      break;
    default:
      break;
    }
}

bool    Nibbler::Snake::eatFruit(Fruit& fruit)
{
  for (size_t i = 0 ; i < _cells.size() ; i++)
    if (fruit.getPos() == _cells[i])
      {
        _nbCells++;
        return (true);
      }
  return (false);
}

std::string     Nibbler::Snake::getStringedLifes(void) const
{
  std::string   lifes = "";

  for (size_t i = 0 ; i < _nbLife ; i++)
    lifes += "*";
  return (lifes);
}

void    Nibbler::Snake::_dead(void)
{
  _cells.clear();
  _cells.push_back(_origPos);
  _speed = pos(0, -1);
  _nbCells = _origCells;
  _nbLife--;
}

bool    Nibbler::Snake::isDead(void)
{
  for (size_t i = 0 ; i < _cells.size() ; i++)
    {
      if (_cells[i].x <= 0 || _cells[i].y <= 0 || _cells[i].x >= _winWidth - 1 || _cells[i].y >= _winHeight - _intHeight - 1)
        {
          _dead();
          break;
        }
      for (size_t j = 0 ; j < _cells.size() ; j++)
        if (_cells[i] == _cells[j] && i != j)
          {
            _dead();
            return (!_nbLife);
          }
    }
  return (!_nbLife);
}

void                    Nibbler::Snake::writeWhereAmI(void) const
{
  arcade::WhereAmI      *wrAmI;

  wrAmI = (arcade::WhereAmI*)(new uint16_t [sizeof(arcade::WhereAmI) + sizeof(arcade::Position) * (_nbCells - 1)]);
  wrAmI->type = arcade::CommandType::WHERE_AM_I;
  wrAmI->lenght = _nbCells;
  for (size_t i = 0 ; i < _nbCells ; i++)
    {
      if (i >= _cells.size())
        {
          wrAmI->position[i].x = _cells[_cells.size() - 1].x;
          wrAmI->position[i].y = _cells[_cells.size() - 1].y;
        }
      else
        {
          wrAmI->position[i].x = _cells[i].x;
          wrAmI->position[i].y = _cells[i].y;
        }
    }
  std::cout.write((char *)wrAmI, sizeof(arcade::WhereAmI) + sizeof(arcade::Position) * (_nbCells));
  delete[] wrAmI;
}

/*
**
** Game public functions
**
*/

Nibbler::Nibbler(ILib* lib): _snake(Snake(pos((_winWidth) / 2, (_winHeight - _intHeight) / 2))),
                             _fruit(Fruit(pos(0, 0)))
{
  _fruit.changePos();
  _lib = lib;
  _score = 0;
  _initLib();
  _end = false;
  _lastMove = ILib::K_UP;
  _begin = clock();
}

Nibbler::~Nibbler(void)
{
  _destroyLib();
}

bool    Nibbler::isGameEnd(void) const
{
  return (_end);
}

void    Nibbler::doTurn(void)
{
  pos   move = pos(0, 0);

  _getEvent();
  if (!_lib)
    {
      _getCmd();
      _snake.specialMove(_cmd);
    }
  switch (_lastMove)
    {
    case ILib::K_UP:
      move.x = 0;
      move.y = -1;
      break;
    case ILib::K_DOWN:
      move.x = 0;
      move.y = 1;
      break;
    case ILib::K_LEFT:
      move.x = -1;
      move.y = 0;
      break;
    case ILib::K_RIGHT:
      move.x = 1;
      move.y = 0;
      break;
    default:
      break;
    }
  _lastMove = ILib::K_NULL;
  if (((double)(clock() - _begin) / (double)(CLOCKS_PER_SEC)) * 100 > 5 || _cmd == arcade::CommandType::PLAY)
    {
      _snake.move(move);
      if (_snake.isDead())
        _end = true;
      if (_snake.eatFruit(_fruit))
        {
          _fruit.changePos();
          _score += 100;
        }
      if (_lib)
        {
          _lib->fillWindow(ILib::DARKGRAY);
          _lib->drawRect(_winWidth - _intWidth, _winHeight - _intHeight, _intWidth, _intHeight, ILib::WHITE);
          _lib->drawRect(0, 0, _winWidth, _winHeight - _intHeight, ILib::LIGHTBLUE);
          _lib->drawText(_winWidth - _intWidth + _intWidth / 10, _winHeight - _intHeight + _intHeight / 10,
                         "Score : ", ILib::LIGHTGRAY, 1);
          _lib->drawText(_winWidth - _intWidth + _intWidth / 10 + 8, _winHeight - _intHeight + _intHeight / 10,
                         std::to_string(_score), ILib::LIGHTRED, 1);
          _lib->drawText(_winWidth - _intWidth + _intWidth / 10, _winHeight - _intHeight + (_intHeight / 10) * 3,
                         "Lifes : ", ILib::LIGHTGRAY, 1);
          _lib->drawText(_winWidth - _intWidth + _intWidth / 10 + 8, _winHeight - _intHeight + (_intHeight / 10) * 3,
                         _snake.getStringedLifes(), ILib::LIGHTCYAN, 1);
        }
      _fruit.draw(_lib);
      _snake.draw(_lib);
      _begin = clock();
    }
  if (_lib)
    _lib->updateWindow();
}

void    Nibbler::setLib(ILib *newLib)
{
  _destroyLib();
  _lib = newLib;
  _initLib();
}

/*
**
** Game privates functions
**
*/

void    Nibbler::_initLib(void)
{
  if (_lib)
    _lib->createWindow(_winWidth, _winHeight, "Nibbler", 20);
}

void    Nibbler::_destroyLib(void)
{
  if (_lib)
    _lib->closeWindow();
}

void            Nibbler::_getEvent(void)
{
  if (!_lib)
    return ;
  ILib::Event   event = _lib->getNextEvent();

  switch (event.key)
    {
    case ILib::K_ESCAPE:
      _end = true;
      return ;
    case ILib::K_UP:
    case ILib::K_DOWN:
    case ILib::K_LEFT:
    case ILib::K_RIGHT:
      _lastMove = event.key;
      break;
    default:
      break;
    }
}

void                    Nibbler::_getCmd(void)
{
  uint16_t              intCmd;
  arcade::GetMap        *map;

  std::cin.read((char *)&intCmd, sizeof(uint16_t));
  _cmd = (arcade::CommandType)intCmd;
  switch (_cmd)
    {
    case arcade::CommandType::WHERE_AM_I:
      _snake.writeWhereAmI();
      break;
    case arcade::CommandType::GET_MAP:
      map = (arcade::GetMap*)(new char [sizeof(arcade::GetMap) + sizeof(arcade::TileType) * (_winWidth * (_winHeight - _intHeight) - 1)]);
      map->type = arcade::CommandType::GET_MAP;
      map->width = _winWidth;
      map->height = (_winHeight - _intHeight);
      memset(map->tile, (int)(arcade::TileType::EMPTY), (map->width * map->height) * sizeof(arcade::TileType));
      map->tile[_fruit.getPos().y * map->width + _fruit.getPos().x] = arcade::TileType::POWERUP;
      for (size_t i = 0 ; i < map->width ; i++)
        {
          map->tile[i] = arcade::TileType::BLOCK;
          map->tile[(map->height - 1) * map->width + i] = arcade::TileType::BLOCK;
        }
      for (size_t i = 0 ; i < map->height ; i++)
        {
          map->tile[i * map->width] = arcade::TileType::BLOCK;
          map->tile[(i + 1) * map->width - 1] = arcade::TileType::BLOCK;
        }
      std::cout.write((char *)map, sizeof(arcade::GetMap) + sizeof(arcade::TileType) * (_winWidth * (_winHeight - _intHeight)));
      delete[] map;
      break;
    default:
      break;
    }
}

/*
**
** extern "C" functions
**
*/

void    Play(void)
{
  GPlay(NULL);
}

void            GPlay(ILib* lib)
{
  Nibbler       game = Nibbler(lib);

  srand(time(NULL));
  while (!game.isGameEnd())
    game.doTurn();
}
