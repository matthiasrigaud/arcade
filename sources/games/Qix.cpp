# include "Qix.hpp"

/*
**
** Init static constant of Qix
**
*/

const size_t    Qix::_winHeight = HEIGHT;
const size_t    Qix::_winWidth = WIDTH;
const size_t    Qix::_intHeight = INT_HEIGHT;
const size_t    Qix::_intWidth = _winWidth;

/*
**
** Operator overloading for pos struct
**
*/

bool    Qix::pos::operator!=(const pos& other) const
{
  return (!(this->x == other.x && this->y == other.y));
}

bool    Qix::pos::operator==(const pos& other) const
{
  return ((this->x == other.x && this->y == other.y));
}

Qix::pos&       Qix::pos::operator=(const pos& other)
{
  this->x = other.x;
  this->y = other.y;
  return (*this);
}

Qix::pos&       Qix::pos::operator+=(const pos& other)
{
  this->x += other.x;
  this->y += other.y;
  return (*this);
}

Qix::pos&       Qix::pos::operator-=(const pos& other)
{
  this->x -= other.x;
  this->y -= other.y;
  return (*this);
}

Qix::pos        Qix::pos::operator+(const pos& other) const
{
  return (pos(this->x + other.x, this->y + other.y));
}

Qix::pos        Qix::pos::operator-(const pos& other) const
{
  return (pos(this->x - other.x, this->y - other.y));
}

/*
**
** BigFuckingMonster functions
**
*/

Qix::BigFuckingMonster::BigFuckingMonster(const pos& p1, const pos& p2, ILib::Colors color, double max): _pos({p1, p2}),
                                                                                                         _speed({pos(-1, -1), pos(-1, 1)})
{
  _color = color;
  _maxSize = max;
}

void    Qix::BigFuckingMonster::draw(ILib* lib) const
{
  lib->drawLine(_pos[0].x, _pos[0].y, _pos[1].x, _pos[1].y, _color);
}

bool            Qix::BigFuckingMonster::_haveCollide(const Map& map)
{
  pos           incr = (_pos[0] - _pos[1]);
  pos           cur;
  double        divX;
  double        divY;

  if (VA(incr.x) > VA(incr.y) && incr.x)
    {
      divX = incr.x / VA(incr.x);
      divY = (double)incr.y / (double)(VA(incr.x));
    }
  else
    {
      divY = incr.y / (VA(incr.y) ? VA(incr.y) : 1);
      divX = (double)incr.x / (double)((VA(incr.y) ? VA(incr.y) : (VA(incr.x) ? VA(incr.x) : 1)));
    }
  for (size_t i = 0 ; (cur = _pos[1] + pos(i * divX, i * divY)) != _pos[0] + pos(divX, divY); i++)
    {
      if (map[cur.y][cur.x] != Qix::CellsType::DRAWABLE &&
          map[cur.y][cur.x] != Qix::CellsType::ANTE_DRAWABLE &&
          map[cur.y][cur.x] != Qix::CellsType::BURNED)
        return (true);
    }
  return (false);
}

void            Qix::BigFuckingMonster::move(const Map& map)
{
  double        dist;

  _pos[0] += _speed[0];
  _pos[1] += _speed[1];
  for (size_t i = 0 ; i < 2 ; i++)
    {
      dist = sqrt(CAR(_pos[0].x - _pos[1].x) + CAR(_pos[0].y - _pos[1].y));
      if (!(_pos[i].y > 0 && _pos[i].y <= (signed)_winHeight - (signed)_intHeight - 2 &&
            _pos[i].x > 0 && _pos[i].x <= (signed)_winWidth - 2) ||
          dist > _maxSize || (rand() % 5) == 4 || _haveCollide(map))
        {
          _pos[i] -= _speed[i];
          _speed[i].x = ((rand() % 3) + 1) * ((rand() % 2) * -2 + 1);
          _speed[i].y = ((rand() % 3) + 1) * ((rand() % 2) * -2 + 1);
        }
    }
}

double                  Qix::BigFuckingMonster::reconquire(Map& map, const pos& orig) const
{
  std::vector<pos>      *stack;
  pos                   cur;
  size_t                nbCellsRemaining = 0;

  stack = new std::vector<pos>;
  stack->push_back(orig);
  while (stack->size())
    {
      cur = stack->back();
      stack->pop_back();
      if (!(cur.y > 0 && cur.y <= (signed)_winHeight - (signed)_intHeight - 2 &&
            cur.x > 0 && cur.x <= (signed)_winWidth - 2) ||
          map[cur.y][cur.x] != Qix::CellsType::BLOCK)
        continue ;
      nbCellsRemaining++;
      map[cur.y][cur.x] = Qix::CellsType::DRAWABLE;
      stack->push_back(pos(cur.x - 1, cur.y));
      stack->push_back(pos(cur.x + 1, cur.y));
      stack->push_back(pos(cur.x, cur.y - 1));
      stack->push_back(pos(cur.x - 1, cur.y - 1));
      stack->push_back(pos(cur.x + 1, cur.y - 1));
      stack->push_back(pos(cur.x - 1, cur.y + 1));
      stack->push_back(pos(cur.x, cur.y + 1));
      stack->push_back(pos(cur.x + 1, cur.y + 1));
    }
  delete stack;
  return ((double(nbCellsRemaining) / double(FIELD_AREA)) * 100);
}

const Qix::pos& Qix::BigFuckingMonster::getPosOne(void) const
{
  return (_pos[0]);
}

const Qix::pos& Qix::BigFuckingMonster::getPosTwo(void) const
{
  return (_pos[1]);
}

void    Qix::BigFuckingMonster::printInterf(arcade::GetMap* map) const
{
  pos           incr = (_pos[0] - _pos[1]);
  pos           cur;
  double        divX;
  double        divY;

  if (VA(incr.x) > VA(incr.y) && incr.x)
    {
      divX = incr.x / VA(incr.x);
      divY = (double)incr.y / (double)(VA(incr.x));
    }
  else
    {
      divY = incr.y / (VA(incr.y) ? VA(incr.y) : 1);
      divX = (double)incr.x / (double)((VA(incr.y) ? VA(incr.y) : (VA(incr.x) ? VA(incr.x) : 1)));
    }
  for (size_t i = 0 ; (cur = _pos[1] + pos(i * divX, i * divY)) != _pos[0] + pos(divX, divY); i++)
    map->tile[cur.y * map->width + cur.x] = arcade::TileType::EVIL_DUDE;
}

/*
**
** PlayerHunter functions
**
*/

Qix::PlayerHunter::PlayerHunter(const pos& startPos, ILib::Colors color): _pos(startPos), _last(pos(-1, -1))
{
  _color = color;
}

void    Qix::PlayerHunter::draw(ILib *lib) const
{
  if (!lib)
    return ;
  lib->fillCell(_pos.x, _pos.y, _color);
}

void                    Qix::PlayerHunter::move(const Map& map)
{
  std::vector<pos>      next;
  std::vector<pos>      valables;

  next.push_back(pos(_pos.x - 1, _pos.y));
  next.push_back(pos(_pos.x + 1, _pos.y));
  next.push_back(pos(_pos.x, _pos.y - 1));
  next.push_back(pos(_pos.x, _pos.y + 1));
  for (size_t i = 0 ; i < next.size() ; i++)
    if (next[i].x >= 0 && next[i].y >= 0 && next[i].x < (signed)_winWidth && next[i].y < (signed)(_winHeight - _intHeight) &&
        map[next[i].y][next[i].x] == Qix::CellsType::WALKABLE && next[i] != _last)
      valables.push_back(next[i]);
  _last = _pos;
  _pos = valables[rand() % valables.size()];
}

const Qix::pos& Qix::PlayerHunter::getPos(void) const
{
  return (_pos);
}

void    Qix::PlayerHunter::printInterf(arcade::GetMap* map) const
{
  map->tile[_pos.y * map->width + _pos.x] = arcade::TileType::EVIL_DUDE;
}

/*
**
** Player functions
**
*/

Qix::Player::Player(const pos& startPos, ILib::Colors color, size_t lifes): _pos(startPos), _orig(startPos)
{
  _color = color;
  _nbLife = lifes;
  _burnColor = ILib::YELLOW;
  _isBurned = false;
  _canBurn = false;
}

void    Qix::Player::draw(ILib *lib) const
{
  if (!lib)
    return ;
  lib->fillCell(_pos.x, _pos.y, _color);
  if (_isBurned)
    lib->fillCell(_burner.x, _burner.y, _burnColor);
}

double          Qix::Player::move(const pos& movement, Map& map, const bool& canTrace, const BFM& qix)
{
  double        newPercent = 0;

  _isBurned = false;
  if (movement == pos(0, 0) && _canBurn)
    {
      _isBurned = true;
      if (map[_burner.y - 1][_burner.x] == Qix::CellsType::ANTE_DRAWABLE || pos(_burner.x, _burner.y - 1) == _pos)
        _burner.y--;
      else if (map[_burner.y][_burner.x - 1] == Qix::CellsType::ANTE_DRAWABLE || pos(_burner.x - 1, _burner.y) == _pos)
        _burner.x--;
      else if (map[_burner.y][_burner.x + 1] == Qix::CellsType::ANTE_DRAWABLE || pos(_burner.x + 1, _burner.y) == _pos)
        _burner.x++;
      else if (map[_burner.y + 1][_burner.x] == Qix::CellsType::ANTE_DRAWABLE || pos(_burner.x, _burner.y + 1) == _pos)
        _burner.y++;
      if (_burner != _pos)
        map[_burner.y][_burner.x] = Qix::CellsType::BURNED;
    }
  if (_pos.y + movement.y >= 0 && _pos.y + movement.y <= (signed)_winHeight - (signed)_intHeight - 1
      && _pos.x + movement.x >= 0 && _pos.x + movement.x <= (signed)_winWidth - 1 &&
      (map[_pos.y + movement.y][_pos.x + movement.x] == Qix::CellsType::WALKABLE ||
      (map[_pos.y + movement.y][_pos.x + movement.x] == Qix::CellsType::DRAWABLE && canTrace)))
    {
      _pos += movement;
      if (map[_pos.y - movement.y][_pos.x - movement.x] == Qix::CellsType::WALKABLE &&
          map[_pos.y][_pos.x] == Qix::CellsType::DRAWABLE)
        {
          _orig = _pos - movement;
          _burner = _orig;
          _canBurn = true;
        }
      if (map[_pos.y - movement.y][_pos.x - movement.x] == Qix::CellsType::DRAWABLE)
        map[_pos.y - movement.y][_pos.x - movement.x] = Qix::CellsType::ANTE_DRAWABLE;
      if ((map[_pos.y - movement.y][_pos.x - movement.x] == Qix::CellsType::ANTE_DRAWABLE ||
           map[_pos.y - movement.y][_pos.x - movement.x] == Qix::CellsType::BURNED) &&
          map[_pos.y][_pos.x] == Qix::CellsType::WALKABLE)
        {
          for (size_t i = 0 ; i < map.size() ; i++)
            for (size_t j = 0 ; j < map[0].size() ; j++)
              if (map[i][j] == Qix::CellsType::ANTE_DRAWABLE || map[i][j] == Qix::CellsType::BURNED)
                map[i][j] = Qix::CellsType::WALKABLE;
              else if (map[i][j] ==  Qix::CellsType::DRAWABLE)
                map[i][j] = Qix::CellsType::BLOCK;
          newPercent = 100.0 - qix.reconquire(map, qix.getPosOne());
          _canBurn = false;
        }
    }
  return (newPercent);
}

double  Qix::Player::specialMove(arcade::CommandType cmd, Map& map, const BFM& qix)
{
  pos   tmp = pos(0, 0);
  bool  haveSpecialMove;

  haveSpecialMove = true;
  switch (cmd)
    {
    case arcade::CommandType::GO_UP:
      tmp.x = 0;
      tmp.y = -1;
      break;
    case arcade::CommandType::GO_DOWN:
      tmp.x = 0;
      tmp.y = 1;
      break;
    case arcade::CommandType::GO_LEFT:
      tmp.x = -1;
      tmp.y = 0;
      break;
    case arcade::CommandType::GO_RIGHT:
      tmp.x = 1;
      tmp.y = 0;
      break;
    default:
      haveSpecialMove = false;
      break;
    }
  return (this->move(tmp, map, haveSpecialMove, qix));
}

std::string     Qix::Player::getStringedLifes(void) const
{
  std::string   lifes = "";

  for (size_t i = 0 ; i < _nbLife ; i++)
    lifes += "*";
  return (lifes);
}

bool            Qix::Player::isDead(Map& map, const BFM& qix, const std::array<PlayerHunter, 2>& sparks)
{
  pos           incr = (qix.getPosTwo() - qix.getPosOne());
  pos           cur;
  double        divX;
  double        divY;

  if (VA(incr.x) > VA(incr.y) && incr.x)
    {
      divX = incr.x / VA(incr.x);
      divY = (double)incr.y / (double)(VA(incr.x));
    }
  else
    {
      divY = incr.y / (VA(incr.y) ? VA(incr.y) : 1);
      divX = (double)incr.x / (double)((VA(incr.y) ? VA(incr.y) : (VA(incr.x) ? VA(incr.x) : 1)));
    }
  for (size_t i = 0 ; i < 2 ; i++)
    if (_pos == sparks[i].getPos())
      return (_dead(map));
  if (_burner == _pos)
    return (_dead(map));
  for (size_t i = 0 ; (cur = qix.getPosOne() + pos(i * divX, i * divY)) != qix.getPosTwo() + pos(divX, divY) ; i++)
    {
      if (map[cur.y][cur.x] == Qix::CellsType::ANTE_DRAWABLE || map[cur.y][cur.x] == Qix::CellsType::BURNED || cur == _pos)
        return(_dead(map));
    }
  return (!_nbLife);
}

bool    Qix::Player::_dead(Map& map)
{
  _nbLife--;
  _pos = _orig;
  for (size_t i = 0 ; i < map.size() ; i++)
    for (size_t j = 0 ; j < map[0].size() ; j++)
      if (map[i][j] == Qix::CellsType::ANTE_DRAWABLE || map[i][j] == Qix::CellsType::BURNED)
        map[i][j] = Qix::CellsType::DRAWABLE;
  _canBurn = false;
  return (!_nbLife);
}

void                    Qix::Player::writeWhereAmI(void) const
{
  arcade::WhereAmI      *wrAmI;

  wrAmI = (arcade::WhereAmI*)(new uint16_t [sizeof(arcade::WhereAmI) + sizeof(arcade::Position)]);
  wrAmI->type = arcade::CommandType::WHERE_AM_I;
  wrAmI->lenght = 1;
  wrAmI->position[0].x = _pos.x;
  wrAmI->position[0].y = _pos.y;
  std::cout.write((char *)wrAmI, sizeof(arcade::WhereAmI) + sizeof(arcade::Position));
  delete[] wrAmI;
}

/*
**
** Game public functions
**
*/

Qix::Qix(ILib* lib): _player(Player(pos(_winWidth / 2, _winHeight - _intHeight - 1))),
                     _qix(BigFuckingMonster(pos(5, 4), pos(10, 2))),
                     _sparks({PlayerHunter(pos(0, (_winHeight - _intHeight - 1) / 2)),
                           PlayerHunter(pos(_winWidth - 1, (_winHeight - _intHeight - 1) / 2))})
{
  _lib = lib;
  _score = 0;
  _initLib();
  _end = false;
  _lastMove = ILib::K_NULL;
  _begin = clock();
  _canTrace = false;
  _cmd = arcade::CommandType::ILLEGAL;
  for (size_t i = 0 ; i < _map.size() ; i++)
    for (size_t j = 0 ; j < _map[0].size() ; j++)
      if (i == 0 || j == 0 || i == _map.size() - 1 || j == _map[0].size() -1)
        _map[i][j] = Qix::CellsType::WALKABLE;
      else
        _map[i][j] = Qix::CellsType::DRAWABLE;
}

Qix::~Qix(void)
{
  _destroyLib();
}

bool    Qix::isGameEnd(void) const
{
  return (_end);
}

void                    Qix::doTurn(void)
{
  pos                   move = pos(0, 0);
  double                newPercent;
  std::ostringstream    oss;

  _getEvent();
  if (!_lib)
    {
      _getCmd();
      _player.specialMove(_cmd, _map, _qix);
    }
  switch (_lastMove)
    {
    case ILib::K_UP:
      move.x = 0;
      move.y = -1;
      break;
    case ILib::K_DOWN:
      move.x = 0;
      move.y = 1;
      break;
    case ILib::K_LEFT:
      move.x = -1;
      move.y = 0;
      break;
    case ILib::K_RIGHT:
      move.x = 1;
      move.y = 0;
      break;
    default:
      break;
    }
  if (((double)(clock() - _begin) / (double)(CLOCKS_PER_SEC)) * 100 > 5 || _cmd == arcade::CommandType::PLAY)
    {
      if ((newPercent = _player.move(move, _map, _canTrace, _qix)))
        {
          _score += (newPercent - _percent) * 100;
          _percent = newPercent;
        }
      _qix.move(_map);
      for (size_t i = 0 ; i < 2 ; i++)
        _sparks[i].move(_map);
      if (_player.isDead(_map, _qix, _sparks))
        _end = true;
      if (_lib)
        {
          _lib->fillWindow(ILib::DARKGRAY);
          for (size_t i = 0 ; i < _map.size() ; i++)
            for (size_t j = 0 ; j < _map[0].size() ; j++)
              _lib->fillCell(j, i, (ILib::Colors)_map[i][j]);
          _lib->drawRect(_winWidth - _intWidth, _winHeight - _intHeight, _intWidth, _intHeight, ILib::WHITE);
          _lib->drawText(_winWidth - _intWidth + _intWidth / 10, _winHeight - _intHeight + _intHeight / 10,
                         "Score : ", ILib::LIGHTGRAY, 1);
          _lib->drawText(_winWidth - _intWidth + _intWidth / 10 + 8, _winHeight - _intHeight + _intHeight / 10,
                         std::to_string(_score), ILib::LIGHTRED, 1);
          _lib->drawText(_winWidth - _intWidth + _intWidth / 10, _winHeight - _intHeight + (_intHeight / 10) * 3,
                         "Lifes : ", ILib::LIGHTGRAY, 1);
          _lib->drawText(_winWidth - _intWidth + _intWidth / 10 + 8, _winHeight - _intHeight + (_intHeight / 10) * 3,
                         _player.getStringedLifes(), ILib::LIGHTCYAN, 1);
          _lib->drawText(_winWidth - _intWidth + _intWidth / 10, _winHeight - _intHeight + (_intHeight / 10) * 5,
                         "Map : ", ILib::LIGHTGRAY, 1);
          oss << std::fixed << std::setprecision(2) << _percent << "%";
          _lib->drawText(_winWidth - _intWidth + _intWidth / 10 + 6, _winHeight - _intHeight + (_intHeight / 10) * 5,
                         oss.str(), ILib::LIGHTRED, 1);
          _player.draw(_lib);
          for (size_t i = 0 ; i < 2 ; i++)
            _sparks[i].draw(_lib);
          _qix.draw(_lib);
        }
      _begin = clock();
    }
  if (_lib)
    _lib->updateWindow();
}

void    Qix::setLib(ILib *newLib)
{
  _destroyLib();
  _lib = newLib;
  _initLib();
}

/*
**
** Game privates functions
**
*/

void    Qix::_initLib(void)
{
  if (_lib)
    _lib->createWindow(_winWidth, _winHeight, "Qix", 20);
}

void    Qix::_destroyLib(void)
{
  if (_lib)
    _lib->closeWindow();
}

void            Qix::_getEvent(void)
{
  if (!_lib)
    return ;
  ILib::Event   event = _lib->getNextEvent();

  switch (event.key)
    {
    case ILib::K_ESCAPE:
      _end = true;
      return ;
    case ILib::K_UP:
    case ILib::K_DOWN:
    case ILib::K_LEFT:
    case ILib::K_RIGHT:
      if (event.type == ILib::E_PRESS)
        _lastMove = event.key;
      else if (event.type == ILib::E_RELEASE)
        _lastMove = ILib::K_NULL;
      break;
    case ILib::K_Z:
      if (event.type == ILib::E_PRESS)
        _canTrace = true;
      else if (event.type == ILib::E_RELEASE)
        _canTrace = false;
      break;
    default:
      break;
    }
}

void                    Qix::_getCmd(void)
{
  uint16_t              intCmd;
  arcade::GetMap        *map;

  std::cin.read((char *)&intCmd, sizeof(uint16_t));
  _cmd = (arcade::CommandType)intCmd;
  switch (_cmd)
    {
    case arcade::CommandType::WHERE_AM_I:
      _player.writeWhereAmI();
      break;
    case arcade::CommandType::GET_MAP:
      map = (arcade::GetMap*)(new char [sizeof(arcade::GetMap) + sizeof(arcade::TileType) * (_winWidth * (_winHeight - _intHeight) - 1)]);
      map->type = arcade::CommandType::GET_MAP;
      map->width = _winWidth;
      map->height = (_winHeight - _intHeight);
      memset(map->tile, (int)(arcade::TileType::EMPTY), (map->width * map->height) * sizeof(arcade::TileType));
      for (size_t i = 0 ; i < map->height ; i++)
        for (size_t j = 0 ; j < map->width ; j++)
          switch (_map[i][j])
            {
            case Qix::CellsType::WALKABLE:
            case Qix::CellsType::DRAWABLE:
              map->tile[i * map->width + j] = arcade::TileType::EMPTY;
              break;
            case Qix::CellsType::BLOCK:
            case Qix::CellsType::ANTE_DRAWABLE:
            case Qix::CellsType::BURNED:
              map->tile[i * map->width + j] = arcade::TileType::BLOCK;
              break;
            }
      _qix.printInterf(map);
      _sparks[0].printInterf(map);
      _sparks[1].printInterf(map);
      std::cout.write((char *)map, sizeof(arcade::GetMap) + sizeof(arcade::TileType) * (_winWidth * (_winHeight - _intHeight)));
      delete[] map;
      break;
    default:
      break;
    }
}

/*
**
** extern "C" functions
**
*/

void    Play(void)
{
  GPlay(NULL);
}

void            GPlay(ILib* lib)
{
  Qix       game = Qix(lib);

  srand(time(NULL));
  while (!game.isGameEnd())
    game.doTurn();
}
